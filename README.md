# Jaco-EarthInfo
Seit deiner letzten Anfrage leben 187.329 mehr Menschen auf der Erde. \
Erfahre was in letzter Zeit auf der Welt alles anders geworden ist.

<br>

What **you can learn** in this skill:
* Webscraping (Reading reloaded informations from websites)

**Complexity**: Medium

<br>

Build and run skill solely for debugging purposes: \
(Assumes mqtt-broker already running) 
```bash
docker build -t skill_jaco_earthinfo_amd64 - < skills/skills/Jaco-EarthInfo/Containerfile_amd64

docker run --network host --rm \
  --volume `pwd`/skills/skills/Jaco-EarthInfo/:/Jaco-Master/skills/skills/Jaco-EarthInfo/:ro \
  --volume `pwd`/skills/skills/Jaco-EarthInfo/skilldata/:/Jaco-Master/skills/skills/Jaco-EarthInfo/skilldata/ \
  --volume `pwd`/../jacolib/:/Jaco-Master/jacolib/:ro \
  --volume `pwd`/userdata/config/:/Jaco-Master/userdata/config/:ro \
  -it skill_jaco_earthinfo_amd64 python3 /Jaco-Master/skills/skills/Jaco-EarthInfo/action-info.py
```

## Sources
- https://www.worldometers.info
