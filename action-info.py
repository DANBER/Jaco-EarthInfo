import datetime
import decimal
import itertools
import json
import os
import random
import threading

import num2words
from bs4 import BeautifulSoup
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from jacolib import assistant

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant

path_facts = file_path + "skilldata/facts.json"
request_running = False
url = "https://www.worldometers.info/"
driver: webdriver.Firefox

fact_keys_nt = ["current_population", "oil_reserves"]
fact_keys_td = [
    "dth1_hunger/today",
    "cellular/today",
    "cigarettes_smoked/today|dth1s_cigarettes/this_year",
    "oil_consumption",
]
fact_keys_ty = [
    "desert_land_formed/this_year",
    "drug_spending/this_year",
    "automobile_produced/this_year|bicycle_produced/this_year",
    "tox_chem/this_year",
]
split_fact_keys: dict


# ==================================================================================================


def load_facts():
    """Load facts from website. Restarts loading if website was not fully loaded"""

    try:
        driver.get(url)
        soup = BeautifulSoup(driver.page_source, "lxml")
        # driver.close()
    except Exception as e:
        print("LoadFacts Error:", e)
        return None

    facts = {"time": str(datetime.datetime.utcnow())}

    for inf in split_fact_keys:
        # Find fact and remove html tags
        name_box = soup.find("span", attrs={"class": "rts-counter", "rel": inf})
        name = name_box.text.strip()

        if name == "retrieving data...":
            # Sometimes page is not fully loaded, try again then
            print("LoadFacts Error:", "Try loading page again")
            facts = load_facts()
            return facts

        val = int(name.replace(",", ""))
        facts[inf] = val

    return facts


# ==================================================================================================


def save_facts(facts):
    # Save facts
    with open(path_facts, "w+") as json_file:
        json.dump(facts, json_file, indent=4)


# ==================================================================================================


def get_fact():
    if os.path.isfile(path_facts):
        with open(path_facts) as json_file:
            old_facts = json.load(json_file)
    else:
        old_facts = None

    facts = load_facts()
    if facts is None:
        return assist.get_random_talk("load_error")

    save_facts(facts)
    language = assist.get_global_config()["language"]

    if old_facts is None:
        info = facts["current_population"]
        info = int(decimal.Context(prec=4).create_decimal(info))
        info = num2words.num2words(info, lang=language)
        text = assist.get_random_talk("current_population").format(info)

    else:
        old_time = datetime.datetime.strptime(old_facts["time"], "%Y-%m-%d %H:%M:%S.%f")
        now_time = datetime.datetime.utcnow()
        if old_time.year == now_time.year and old_time.day == now_time.day:
            fact_keys = fact_keys_nt + fact_keys_ty + fact_keys_td
        elif old_time.year == now_time.year:
            fact_keys = fact_keys_nt + fact_keys_ty
        else:
            fact_keys = fact_keys_nt

        fact = random.choice(fact_keys)
        text = assist.get_random_talk("facts_start")

        if "|" not in fact:
            info = facts[fact] - old_facts[fact]
            info = int(decimal.Context(prec=3).create_decimal(info))
            info = num2words.num2words(info, lang=language)
            text = text + random.choice(assist.get_talks()["facts"][fact]).format(info)
        else:
            fs = fact.split("|")
            text = text + random.choice(assist.get_talks()["facts"][fact])
            infos = []
            for f in fs:
                info = facts[f] - old_facts[f]
                info = int(decimal.Context(prec=3).create_decimal(info))
                info = num2words.num2words(info, lang=language)
                infos.append(info)
            text = text.format(*infos)

    return text


# ==================================================================================================


def say_fact(message):
    global request_running

    result_sentence = get_fact()
    request_running = False
    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_tell_fact(message):
    """Callback to handle tell request"""
    global request_running

    if not request_running:
        request_running = True
        result_sentence = assist.get_random_talk("wait_for_answer")

        # Extra non blocking thread
        thread = threading.Thread(target=say_fact, args=(message,))
        thread.start()

    else:
        result_sentence = assist.get_random_talk("request_running")

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def load_webdriver():
    global driver

    # Virtual display is needed for geckodriver
    display = Display(visible=0, size=(800, 600))
    display.start()

    options = Options()
    options.headless = True
    driver = webdriver.Firefox(options=options, log_path="/tmp/geckodriver.log")


# ==================================================================================================


def split_keys(facts):
    """Combine keys types and split keys containing two keys,
    separated keys are needed to update facts"""

    all_keys = list(facts.keys())
    splittable = [f for f in all_keys if "|" in f]
    split_fks = list(set(all_keys) - set(splittable))
    split = [str(f).split("|") for f in splittable]
    split = list(itertools.chain(*split))
    split_fks = split_fks + split
    return split_fks


# ==================================================================================================


def main():
    global assist, split_fact_keys

    assist = assistant.Assistant(repo_path=file_path)
    load_webdriver()
    split_fact_keys = split_keys(assist.get_talks()["facts"])

    assist.add_topic_callback("tell_fact", callback_tell_fact)
    assist.run()


# ==================================================================================================

if __name__ == "__main__":
    main()
